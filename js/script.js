$(document).ready(function() {
	$('.main-slider').slick({
		dots: true,
		infinite: false,
		speed: 300,
		slidesToShow: 1,
		slidesToScroll: 1,
		appendArrows: $(".slider-container .buttons"),
 
		responsive: [
		{
			breakpoint: 990,
			settings: {
				arrows: false,
			}
		},
		{
			breakpoint: 500,
			settings: {
				arrows: false,
				dots: false
			}
		}
		// You can unslick at a given breakpoint now by adding:
		// settings: "unslick"
		// instead of a settings object
		]
	});
	$('.main-slider').on('afterChange', function(event, slick, currentSlide){ 
		$('.main-slider').slick('slickPause'); $('#video')[0].play();
	});
	
	$('.top-item-slider').slick({
		dots: false,
		infinite: false,
		speed: 300,
		slidesToShow: 3,
		slidesToScroll: 1,
		arrows: false,
		responsive: [
		{
			breakpoint: 990,
			settings: {
				arrows: false,
				slidesToShow: 1,
				dots: true,
			}
		}
		]
	});
	
	$(".catalog-block aside .item-info").css("height", $('.main-item').outerHeight()/2.43);
	$(window).resize(function() {
		$(".catalog-block aside .item-info").css("height", $('.main-item').outerHeight()/2.43);
	});
	
	$(".catalog-block aside .small-item").css("height", $('.main-item').outerHeight()/1.85);
	$(window).resize(function() {
		$(".catalog-block aside .small-item").css("height", $('.main-item').outerHeight()/1.85);
	});

	$("#drop, nav").click(function() {
		$(".accordion").toggleClass("open");
		$(".top-nav-bg").toggleClass("active");
		/*$(this).toggleClass("open");*/
		$(window).scrollTop(0); //cheating
		$(".search-form").slideUp("fast");
		$('.overflow').toggleClass('active');
	});
	
$( function()
{
    var targets = $( '[rel~=tooltip]' ),
        target  = false,
        tooltip = false,
        title   = false;
 
    targets.bind( 'mouseenter', function()
    {
        target  = $( this );
        tip     = target.attr( 'title' );
        tooltip = $( '<div id="tooltip"></div>' );
 
        if( !tip || tip == '' )
            return false;
 
        target.removeAttr( 'title' );
        tooltip.css( 'opacity', 0 )
               .html( tip )
               .appendTo( 'body' );
 
        var init_tooltip = function()
        {
            if( $( window ).width() < tooltip.outerWidth() * 1.5 )
                tooltip.css( 'max-width', $( window ).width() / 2 );
            else
                tooltip.css( 'max-width', 340 );
 
            var pos_left = target.offset().left + ( target.outerWidth() / 2 ) - ( tooltip.outerWidth() / 2 ),
                pos_top  =  tooltip.outerHeight() - target.offset().top +15;
 
            if( pos_left < 0 )
            {
                pos_left = target.offset().left + target.outerWidth() / 2 - 20;
                tooltip.addClass( 'left' );
            }
            else
                tooltip.removeClass( 'left' );
 
            if( pos_left + tooltip.outerWidth() > $( window ).width() )
            {
                pos_left = target.offset().left - tooltip.outerWidth() + target.outerWidth() / 2 + 20;
                tooltip.addClass( 'right' );
            }
            else
                tooltip.removeClass( 'right' );
 
            if( pos_top < 0 )
            {
                var pos_top  = target.offset().top + target.outerHeight() + 15;
                tooltip.addClass( 'top' );
            }
            else
                tooltip.removeClass( 'top' );
 
            tooltip.css( { left: pos_left, top: pos_top } )
                   .animate( { top: '+=10', opacity: 1 }, 50 );
        };
 
        init_tooltip();
        $( window ).resize( init_tooltip );
 
        var remove_tooltip = function()
        {
			
            tooltip.animate( { top: '-=10', opacity: 0 }, 50, function()
            {
                $( this ).remove();
            });
 
            target.attr( 'title', tip );
        };
 
        target.bind( 'mouseleave', remove_tooltip );
        tooltip.bind( 'click', remove_tooltip );
    });
});
	
	$('header .nav-group').each(function() {
		var $this = $(this);
		if ($this.find('.nav-image').length > 4) { //if looking for direct descendants then do .children('div').length
			$this.addClass('align-left');
		}
	});
	
	$(".top-nav .search-button, .search-form .head-form .close-icon").on("click", function(e){
		e.preventDefault();
		$(".search-form").slideToggle("slow");
		$(this).toggleClass("active")
 		
	});
	
	$(".search-form .head-form .close-icon").on("click", function(e){
		$('.overflow').removeClass('active');
		$(".body-form").slideUp("slow");
	});
	

	$( ".accordion-tab" ).accordion({
		collapsible: true,
		active: false,
		animate: 400,
		heightStyle: 'content',
		header: '.group-title',
		autoHeight: true,
		clearStyle: true,
		navigation: true
	});

		

	 $(".filter-box > a").on("click", function() {
	    if ($(this).hasClass("active")) {
	      $(this).removeClass("active");
	      $(this)
	        .siblings(".content")
	        .slideUp(200);
	      $(".filter-box > a i")
	        .removeClass("fa-minus")
	        .addClass("fa-plus");
	    } else {
	      $(".filter-box > a i")
	        .removeClass("fa-minus")
	        .addClass("fa-plus");
	      $(this)
	        .find("i")
	        .removeClass("fa-plus")
	        .addClass("fa-minus");
	      $(".filter-box > a").removeClass("active");
	      $(this).addClass("active");
	      $(".content").slideUp(200);
	      $(this)
	        .siblings(".content")
	        .slideDown(200);
	    }
	  });



		
    $(".catalog-img").on("beforeChange", function(event, slick) {
      var currentSlide, slideType, player, command;
      
      //find the current slide element and decide which player API we need to use.
      currentSlide = $(slick.$slider).find(".slick-current");
      
      //determine which type of slide this, via a class on the slide container. This reads the second class, you could change this to get a data attribute or something similar if you don't want to use classes.
      slideType = currentSlide.attr("class").split(" ")[1];
      
      //get the iframe inside this slide.
      player = currentSlide.find("iframe").get(0);
      
      if (slideType == "vimeo") {
        command = {
          "method": "pause",
          "value": "true"
        };
      } else {
        command = {
          "event": "command",
          "func": "pauseVideo"
        };
      }
      
      //check if the player exists.
      if (player != undefined) {
        //post our command to the iframe.
        player.contentWindow.postMessage(JSON.stringify(command), "*");
      }
    });

 $('.catalog-img').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: false,
  fade: true,
  asNavFor: '.catalog-img-nav'
});
$('.catalog-img-nav').slick({
  slidesToShow: 6,
  slidesToScroll: 1,
  asNavFor: '.catalog-img',
  dots: false,
  useCSS:false,
  useTransform:false,
  focusOnSelect: true,
		responsive: [
		{
			breakpoint: 500,
			settings: {
				slidesToShow: 5,
				arrows: false,
				dots: false
			}
		}
		// You can unslick at a given breakpoint now by adding:
		// settings: "unslick"
		// instead of a settings object
		]
});
 
$('[data-fancybox="gallery"]').fancybox({
  thumbs : {
    autoStart : true,
    axis      : 'x'
  },
  baseClass: 'gallery',
    buttons: [
        "close"
    ],

});

$('.popup').fancybox({
	animationEffect: "fade",
	touch: false
});


// radio buttons

$('input, select').styler();
$('input:radio').change(function() {
	var n = $(this).attr('name');
	var nm = $('input:radio[name='+n+']').not(':checked').closest('label.checked');
	if( nm )
	nm.removeClass('checked');
	$(this).closest('label').addClass('checked');
});


$('.second-section-radio input[name="radio"]').on('change', function() {
	if ($('#radio-1, #radio-2, #radio-3, #radio-4').is(':checked')) {
		$(this).parent().parent().parent().parent().find('.adress-block').slideUp(350);
		$(this).parent().parent().parent().next('.adress-block').slideDown(350);
	} else {
	};
});

$('.third-section-radio input[name="radio1"]').on('change', function() {
	if ($('#radio-11, #radio-12, #radio-13, #radio-14, #radio-15').is(':checked')) {
		$(this).parent().parent().parent().parent().find('.adress-block').slideUp(350);
		$(this).parent().parent().parent().next('.adress-block').slideDown(350);
	} else {
	};
});

//////////////


$.ionTabs("#tabs_1", {
    type: "storage",                    // hash, storage or none
});

$.ionTabs("#location-tab", {
    type: "storage",                    // hash, storage or none
});

$('.toggle').click(function(e) {
  	e.preventDefault();
  
    var $this = $(this);
  
    if ($this.next().hasClass('show')) {
        $this.next().removeClass('show');
        $this.next().slideUp(350);
		$this.removeClass('active');
    } else {
        $this.parent().parent().find('li .inner').removeClass('show');
        $this.parent().parent().find('li .inner').slideUp(350);
        $this.next().toggleClass('show');
        $this.next().slideToggle(350);
		$this.addClass('active');
    }
});


	$('.pro-button').click(function() {
		$(this).toggleClass('open');
		$(this).parent().parent().find('.pro-block').slideToggle("fast");	
		if ($(this).html() == "Активировать промокод") { 
			$(this).html("Активировать промокод<div class='close'><span></span><span></span></div>"); 
		} else { 
			$(this).html("Активировать промокод"); 
		}; 
		return false;
	});	


if ($("body").hasClass("cart-page")) {
    $('header .logo').clone().insertBefore('.top-nav .left-side ul');
	$('header .cart-block').clone().insertBefore('.top-nav .right-side .search-button');
}



/*placeholder*/
(function($) {
  $.fn.phAnim = function( options ) {

    // Set default option
    var settings = $.extend({}, options),
    		label,
  			ph;
    
    // get label elem
    function getLabel(input) {
      return $(input).parent().find('label');
    }
    
    // generate an id
    function makeid() {
      var text = "";
      var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
      for( var i=0; i < 5; i++ )
        text += possible.charAt(Math.floor(Math.random() * possible.length));
      return text;
    }
    
    return this.each( function() {
			
      // check if the input has id or create one
      if( $(this).attr('id') == undefined ) {
        $(this).attr('id', makeid());
      }

      // check if elem has label or create one
      if( getLabel($(this)).length == 0 ) {
        // check if elem has placeholder
        if( $(this).attr('placeholder') != undefined ) {
          ph = $(this).attr('placeholder');
          $(this).attr('placeholder', '');
          // create a label with placeholder text
          $(this).parent().prepend('<label for='+ $(this).attr('id') +'>'+ ph +'</label>');
        }
      } else {
        // if elem has label remove placeholder
        $(this).attr('placeholder', '');
        // check label for attr or set it
        if(getLabel($(this)).attr('for') == undefined ) {
          getLabel($(this)).attr('for', $(this).attr('id'));
        }
      }

      $(this).on('focus', function() {
        label = getLabel($(this));
        label.addClass('active focusIn');
      }).on('focusout', function() {
        if( $(this).val() == '' ) {
          label.removeClass('active');
        }
        label.removeClass('focusIn');
      });
    });
  };
}(jQuery));

 
	$('.input-parent input').phAnim();
 
/*placeholder end*/

 

	// $("#popup-cheaper").validate({
	// 	rules: {
	// 		url: {
	// 			required: true				
	// 		},
	// 		phone: {
	// 			required: true
	// 		},
	// 	},
	// 	messages: {
 // 				url: {
	// 				required: "Укажите ссылку на товар",
	// 			},
	// 		phone: "Заполните телефон, чтобы мы могли оперативно связаться с вами",
	// 	},
	// 	submitHandler: function() {
	// 		parent.$.fancybox.close();
	// 		$.fancybox.open({
	// 			src  : '#success',
	// 			type : 'inline',
	// 		});
	// 	}
	// });
		
	// $("#popup-express").validate({
	// 	rules: {
	// 		phone: {
	// 			required: true
	// 		},
	// 	},
	// 	messages: {
	// 		phone: "Заполните телефон, чтобы мы могли оперативно связаться с вами",
	// 	},
	// 	submitHandler: function() {
	// 		parent.$.fancybox.close();
	// 		$.fancybox.open({
	// 			src  : '#success',
	// 			type : 'inline',
	// 			opts : {
	// 				beforeLoad : function( instance, current ) {
	// 					$('#success').find('.title').text('Заявка отправлена');
	// 					$('#success').find('.result-text').text('В ближайшее время мы свяжемся с вами, чтобы оформить заказ');
	// 				}
	// 			}
 
	// 		});
	 
	// 	}
	// });
		
	function sendForm(formId) {
		var form = $('form#' + formId);
		var msg  = form.serialize();
		var required = form.find('[required="true"]');
		var error = false;
		for(var i = 0; i <= (required.length - 1);i++){
		    if(required[i].value == ''){
				required[i].style.borderColor = 'rgba(255, 0, 0, 1)';
				error = true;
			}
		}
		if (error){
		    return false;
		} else {
			$.ajax({
				type: 'POST',
				data: msg,
				success: function(data) {
					form.parents('.modal_form').addClass('sent');
					$('.modal_form').removeClass('visible');
					$('.modal_overlay').addClass('visible');
					$('#success').attr('style','margin-top:' + ($(window).scrollTop() + 120) + 'px').addClass('visible');
					console.log(formId);
					
				},
				error: function(xhr, str){
				    form.html('Возникла ошибка: ' + xhr.responseCode);
				}
			});
		}
	}
	
	$('form button').click(function () {
		var form = $(this).parent().parent().parent().find('form').attr('id');
		sendForm(form);
		//alert(form);
	});
	




// popup map
    var overlay = $('#overlay');
    var open_modal = $('.open_modal');
    var close = $('.modal_close, #overlay');
    var modal = $('#modal-map');

     open_modal.click( function(event){
     	console.log("xyu");
         event.preventDefault();
         var div = $(this).attr('href');
         overlay.fadeIn(400,
             function(){
                 $(div) 
                     .css('display', 'block') 
                     .animate({opacity: 1, top: '50%'}, 200);
         });
     });

     close.click( function(){
            modal 
             .animate({opacity: 0, top: '45%'}, 200, 
                 function(){ 
                     $(this).css('display', 'none');
                     overlay.fadeOut(400); 
                 }
             );
     });

// ****


$(".custom-select").each(function() {
  var classes = $(this).attr("class"),
      id      = $(this).attr("id"),
      name    = $(this).attr("name");
  var template =  '<div class="' + classes + '">';
      template += '<span class="custom-select-trigger">Выберите пункт</span>';
      template += '<div class="custom-options">';
      $(this).find("option").each(function() {
        template += '<span class="custom-option ' + $(this).attr("class") + '" data-value="' + $(this).attr("value") + '">' + $(this).html() + '</span>';
      });
  template += '</div></div>';
  
  $(this).wrap('<div class="custom-select-wrapper"></div>');
  $(this).hide();
  $(this).after(template);
});
$(".custom-option:first-of-type").hover(function() {
  $(this).parents(".custom-options").addClass("option-hover");
}, function() {
  $(this).parents(".custom-options").removeClass("option-hover");
});
$(".custom-select-trigger").on("click", function() {
  $('html').one('click',function() {
    $(".custom-select").removeClass("opened");
  });
  $(this).parents(".custom-select").toggleClass("opened");
  event.stopPropagation();
});
$(".custom-option").on("click", function() {
  $(this).parents(".custom-select-wrapper").find("select").val($(this).data("value"));
  $(this).parents(".custom-options").find(".custom-option").removeClass("selection");
  $(this).addClass("selection");
  $(this).parents(".custom-select").removeClass("opened");
  $(this).parents(".custom-select").find(".custom-select-trigger").text($(this).text());
});



$( ".group-title" ).hover(function() {
	$(".overflow1").toggleClass("overflow1-block");
});

// $('#drop').on("click", function() {
// 	$(".overflow1").toggleClass("overflow1-block");
// });
 
});